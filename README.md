# Git tutorial

## Configuring platform
- For windows down git bash from https://gitforwindows.org/
- For linux `apt-get install git`

### Configure git User
 
| Windows | Linux |
| ------ | ------ |
| Right click on a blank space and select `Git Bash Here`, this will open a git terminal | Use the tradition commandline |

Execute the below commands with your Username and Email address :
```sh
  
  git config --global user.name "User"
  
  git config --global user.email "user@example.com"
  
```


## Managing Repositories
To download a repository from bitbucket, first go to any directory in your local  machine and open a terminal there (**windows git bash** / **linux terminal**).
Now go to bitbucket repository that you want to clone to your local machine. Look for the clone button and click. This will open a window with auto selected clone command. copy the command and paste it in the terminal and __Enter__.
It will take some time to clone the repository depending on the size of the repository. Once finished, go to the newly cloned directory and you will see the current branch name in the git terminal. At first it is always `master` branch.
### Branch Ethics
  - `develop` used test all the changes and then merge with master for deployment
  - `XXXX` used to make new changes, test and merge to `develop` for integration testing
  - `feature-XXXX` used to create new feature, test and merge with develop for integration testing
  How to execute merging has been discussed later [paragraph](#merging).
### Merging
  `Merging` one branch can be done by 2 ways
  - using `git merge` command 
``` sh
# First go to the target branch by executing checkout command
$ git checkout targetbranch
# Now merge the updated branch to the target branch
$ git merge updatedbranch
```